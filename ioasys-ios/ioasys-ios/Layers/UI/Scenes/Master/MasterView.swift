//
//  MasterView.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 28/01/21.
//

import SwiftUI

struct MasterView: View {
    @EnvironmentObject var userSessionService: WebUserSessionService
    @ObservedObject var model: MasterViewModel

    var body: some View {
        sendUserToView()
    }
    
    func sendUserToView() -> AnyView {
        if model.hasUserSession {
            return AnyView(
                HomeView(model: .init(
                            authHeaders: userSessionService.session
                ))
            )
        } else {
            return AnyView(SignInView(model: .init()))
        }
    }
}

struct MasterView_Previews: PreviewProvider {
    static var previews: some View {
        MasterView(model: .init(userSessionService: .init()))
    }
}
