//
//  Font+Extension.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 29/01/21.
//

import Foundation
import SwiftUI

extension Font {
    
public static var largeTitle: Font {
    return Font.custom("Rubik", size: UIFont.preferredFont(forTextStyle: .largeTitle).pointSize)
}

public static var title: Font {
    return Font.custom("Rubik", size: UIFont.preferredFont(forTextStyle: .title1).pointSize)
}

public static var headline: Font {
    return Font.custom("Rubik", size: UIFont.preferredFont(forTextStyle: .headline).pointSize)
}

public static var subheadline: Font {
    return Font.custom("Rubik", size: UIFont.preferredFont(forTextStyle: .subheadline).pointSize)
}

public static var body: Font {
       return Font.custom("Rubik", size: UIFont.preferredFont(forTextStyle: .body).pointSize)
   }

public static var callout: Font {
       return Font.custom("Rubik", size: UIFont.preferredFont(forTextStyle: .callout).pointSize)
   }

public static var footnote: Font {
       return Font.custom("Rubik", size: UIFont.preferredFont(forTextStyle: .footnote).pointSize)
   }

public static var caption: Font {
       return Font.custom("Rubik", size: UIFont.preferredFont(forTextStyle: .caption1).pointSize)
   }

public static func system(size: CGFloat, weight: Font.Weight = .regular, design: Font.Design = .default) -> Font {
    var font = "Rubik"
    switch weight {
    case .bold: font = "Rubik-Bold"
    case .heavy: font = "Rubik-ExtraBold"
    case .light: font = "Rubik-Light"
    case .medium: font = "Rubik-Regular"
    case .semibold: font = "Rubik-SemiBold"
    case .thin: font = "Rubik-Light"
    case .ultraLight: font = "Rubik-Light"
    default: break
    }
    return Font.custom(font, size: size)
}
}
