//
//  NavigationButton.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 26/01/21.
//

import SwiftUI

struct CustomRoundedRectangle: View {
    var body: some View {
        RoundedRectangle(cornerRadius: 8)
            .fill(Color.black.opacity(0.2))
    }
}

struct CustomButton: View {
    var title: String = ""
    var action: () -> Void
    let roundedRectangle = CustomRoundedRectangle()
    @State var didTap: Bool = false
    
    var body: some View {
        ZStack {
            Text(title)
                .font(.subheadline)
                .bold()
            
            if didTap {
                roundedRectangle
            } else {
                roundedRectangle.hidden()
            }
        }
        .frame(
            minWidth: 100, idealWidth: .infinity,
            maxWidth: .infinity, minHeight: 40,
            idealHeight: 48, maxHeight: 50,
            alignment: .center
        )
        .background(
            RoundedRectangle(cornerRadius: 8)
                .fill(Color("Button"))
        )
        .foregroundColor(.white)
        .onTapGesture {
            self.didTap = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.didTap = false
            }
            action()
        }
    }
}

struct CustomButton_Previews: PreviewProvider {
    static var previews: some View {
        CustomButton(title: "ENTRAR", action: {})
    }
}
