//
//  MasterViewModel.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 28/01/21.
//

import Combine
 
final class MasterViewModel: ObservableObject {
    @Published var hasUserSession: Bool
    private var cancellable: AnyCancellable?
    
    init(userSessionService: WebUserSessionService) {
        hasUserSession = userSessionService.session != nil
        cancellable = userSessionService.$session
            .map { $0 != nil }
            .removeDuplicates()
            .assign(to: \.hasUserSession, on: self)
    }
}
