//
//  CustomSecureField.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 26/01/21.
//

import SwiftUI

struct CustomSecureField: View {
    @Binding var text: String
    @Binding var shouldDisplayError: Bool
    
    var body: some View {
        HStack(spacing: 0) {
            SecureField("", text: $text)
                .padding()
                .onAppear() {
                    UITextField.appearance().tintColor = UIColor(named: "TextFieldCursor")
                }

                        
            Image(systemName: shouldDisplayError ? "xmark.circle.fill" :  "eye.fill")
                .foregroundColor(
                    Color(shouldDisplayError ? .red : .darkGray)
                )
                .padding(.trailing)
        }.background(
            RoundedRectangle(cornerRadius: 4)
                .fill(Color("TextFieldBackground"))
        )
        .overlay(
            RoundedRectangle(cornerRadius: 4)
                .stroke(Color.red, lineWidth: shouldDisplayError ? 1 : 0)
        )
    }
}

struct CustomSecureField_Previews: PreviewProvider {
    static var previews: some View {
        CustomSecureField(text: .constant(""), shouldDisplayError: .constant(false))
            .previewLayout(.sizeThatFits)
    }
}
