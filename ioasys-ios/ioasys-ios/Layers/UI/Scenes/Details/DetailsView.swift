//
//  DetailsView.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 29/01/21.
//

import SwiftUI

struct DetailsView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var enterpriseName: String = ""
    var description: String = ""
    
    var btnBack : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) {
        CustomBackButton()
    }
    }
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false){
            VStack {
                EnterpriseCell(enterpriseName: enterpriseName)
                Text(description)
                    .font(.custom("Rubik", size: 20))
                    .fontWeight(.light)
                    .padding()
            }
            .padding(.top)
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: btnBack)
        .navigationTitle(
            Text(enterpriseName)
        )
        .navigationBarTitleDisplayMode(.inline)
        .onAppear() {
            UINavigationBar.appearance().barTintColor = .clear
            UINavigationBar.appearance().backgroundColor = .clear
            UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
            UINavigationBar.appearance().shadowImage = UIImage()
        }
    }
}

struct DetailsView_Previews: PreviewProvider {
    static var previews: some View {
        DetailsView(enterpriseName: "Empresa X", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dignissim ligula at diam pretium, sed efficitur quam accumsan. Phasellus blandit, neque non ultricies egestas, ligula quam blandit est, sed maximus justo nisl ac nunc. Curabitur vitae auctor felis, non dignissim mi. Nam a consequat turpis. Nulla placerat nibh et eros elementum, sed aliquet metus auctor. Fusce egestas mollis metus, nec porttitor ex consectetur ac. Etiam vulputate libero porttitor neque venenatis elementum. Donec mattis sollicitudin lorem a aliquam. Maecenas nec dolor id est sagittis porttitor at ac ipsum. Sed id lobortis ante. Proin semper augue at nibh semper dapibus. Praesent ultrices ut justo id tincidunt.")
    }
}
