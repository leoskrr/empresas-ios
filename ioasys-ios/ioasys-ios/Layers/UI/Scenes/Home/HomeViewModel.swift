//
//  HomeViewModel.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 28/01/21.
//

import Foundation

final class HomeViewModel: ObservableObject {
    var enterpriseService: EnterpriseService
    var authHeaders: UserSession?
    
    @Published var searchText: String = "" {
        didSet {
            if searchText.count > 0 {
                searchEnterprises()
            }
        }
    }
    @Published var enterprises: [Enterprise] = []
    @Published var isDataLoading: Bool = false

    
    init(enterpriseService: EnterpriseService = WebEnterpriseService(), authHeaders: UserSession? = nil) {
        self.enterpriseService = enterpriseService
        self.authHeaders = authHeaders
    }
    
    func searchEnterprises(){
        if let headers = authHeaders {
            isDataLoading = true
            
            enterpriseService.find(byCharacters: searchText, authHeaders: headers) {
                findEnterprises in
                
                self.enterprises = findEnterprises
                self.isDataLoading = false
            }
        }
    }

}
