//
//  CustomBackButton.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 29/01/21.
//

import SwiftUI

struct CustomBackButton: View {
    var body: some View {
        Image(systemName: "arrow.left")
            .foregroundColor(Color("Button"))
            .padding(15)
            .background(
                RoundedRectangle(cornerRadius: 4)
                    .fill(Color("TextFieldBackground"))
            )
    }
}

struct CustomBackButton_Previews: PreviewProvider {
    static var previews: some View {
        CustomBackButton().previewLayout(.sizeThatFits)
    }
}
