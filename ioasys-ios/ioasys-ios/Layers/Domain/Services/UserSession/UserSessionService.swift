//
//  UserSessionService.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 28/01/21.
//

import Foundation

enum SignInResponse {
    case success
    case error(statusCode: Int)
}

protocol UserSessionService {
    func signUpWith(email: String, password: String, completionHandler: @escaping (SignInResponse) -> ())
}
