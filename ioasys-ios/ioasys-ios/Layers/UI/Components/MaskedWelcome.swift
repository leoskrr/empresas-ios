//
//  MaskedWelcome.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 26/01/21.
//

import SwiftUI

struct MaskedWelcome: View {
    var body: some View {
        ZStack {
            Image("MaskedBg")
                .resizable()
                .scaledToFill()
                
                .frame(minWidth: 0,
                       maxWidth: .infinity,
                       minHeight: 50,
                       maxHeight: 220,
                       alignment: .center)
            
            VStack(spacing: 20) {
                Image("LogoHome")
                
                Text("Seja bem vindo ao empresas!")
                    .foregroundColor(.white)
            }
        }
    }
}

struct MaskedWelcome_Previews: PreviewProvider {
    static var previews: some View {
        MaskedWelcome().previewLayout(.sizeThatFits)
    }
}
