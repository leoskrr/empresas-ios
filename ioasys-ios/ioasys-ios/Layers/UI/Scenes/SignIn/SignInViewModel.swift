//
//  SignInViewModel.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 26/01/21.
//

import SwiftUI

final class SignInViewModel: ObservableObject {
    @Published private(set) var state: SignInViewState
    var userSessionService: UserSessionService
    
    var bindings: (
        emailTxt: Binding<String>,
        passwordTxt: Binding<String>,
        isCredentialsWrong: Binding<Bool>,
        isDataLoading: Binding<Bool>
    ) {
        (
            emailTxt: Binding(
                get: { self.state.emailTxt },
                set: { self.state.emailTxt = $0 }
            ),
            passwordTxt: Binding(
                get: { self.state.passwordTxt },
                set: { self.state.passwordTxt = $0 }
            ),
            isCredentialsWrong: Binding(
                get: { self.state.isCredentialsWrong },
                set: { self.state.isCredentialsWrong = $0}
            ),
            isDataLoading: Binding(
                get: { self.state.isDataLoading },
                set: { self.state.isDataLoading = $0}
            )
        )
    }
    
    init(initialState: SignInViewState = .init(),
         userSessionService: UserSessionService = WebUserSessionService()) {
        state = initialState
        self.userSessionService = userSessionService
    }
    
    func submitSignIn(){
        state.isDataLoading = true

        userSessionService.signUpWith(email: state.emailTxt, password: state.passwordTxt){
            response in
            
            switch response {
            case .success:
                break
            case let .error(code):
                if code == 401 {
                    self.state.isCredentialsWrong = true
                }
            }
            
            self.state.isDataLoading = false
        }
    }
}
