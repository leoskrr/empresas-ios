//
//  Enterprise.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 27/01/21.
//

import Foundation

struct Enterprises: Codable {
    var enterprises: [Enterprise]
}

struct EnterpriseType: Codable {
    public var id: Int
    public var enterprise_type_name: String
}

struct Enterprise: Codable, Identifiable {
    var id: Int
    var enterprise_name: String
    var description: String
    var twitter: String?
    var linkedin: String?
    var phone: String?
    var own_enterprise: Bool
    var photo: String
    var share_price: Double
    var enterprise_type: EnterpriseType
    var city: String
    var email_enterprise: String?
    var facebook: String?
    var country: String
    var value: Int
}
