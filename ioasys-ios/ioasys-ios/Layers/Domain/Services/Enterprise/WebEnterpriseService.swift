//
//  WebEnterpriseService.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 28/01/21.
//

import Foundation
import Alamofire

final class WebEnterpriseService: EnterpriseService {
    init() {}
    
    func find(byCharacters characters: String, authHeaders: UserSession, completionHandler: @escaping ([Enterprise]) -> ()) {
        let headers: HTTPHeaders = [
            .init(name: "access-token", value: authHeaders.accessToken),
            .init(name: "client", value: authHeaders.client),
            .init(name: "uid", value: authHeaders.uid)
        ]
        
        AF.request("\(apiUrl)/enterprises?name=\(characters)", method: .get, headers: headers)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseData{
                response in
                switch response.result {
                case let .success(data):
                    do {
                        let enterprises = try JSONDecoder().decode(Enterprises.self, from: data)
                        completionHandler(enterprises.enterprises)
                    } catch {
                        completionHandler([])
                    }
                case .failure:
                    completionHandler([])
                }
            }
    }
}
