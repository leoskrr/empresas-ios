//
//  EnterpriseService.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 28/01/21.
//

import Foundation

protocol EnterpriseService {
    func find(byCharacters characters: String, authHeaders: UserSession, completionHandler: @escaping ([Enterprise]) -> ())
}
