//
//  SignInView.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 26/01/21.
//

import SwiftUI

struct SignInView: View {
    @EnvironmentObject var userSessionService: WebUserSessionService
    @ObservedObject var model: SignInViewModel
    
    init(model: SignInViewModel) {
        self.model = model
    }
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading, spacing: 0) {
                MaskedWelcome()
                    .padding(.bottom)
                    .ignoresSafeArea()
                
                Text("Email")
                    .foregroundColor(Color("TextFieldLabel"))
                    .font(.subheadline)
                    .padding(.horizontal)
                    .padding(.bottom, 5)
                
                CustomTextField(
                    text: model.bindings.emailTxt, shouldDisplayError: model.bindings.isCredentialsWrong)
                    .padding(.horizontal)
                    .padding(.bottom, 15)
                    .keyboardType(.emailAddress)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                
                Text("Senha")
                    .foregroundColor(Color("TextFieldLabel"))
                    .font(.subheadline)
                    .padding(.horizontal)
                    .padding(.bottom, 5)
                
                CustomSecureField(text: model.bindings.passwordTxt, shouldDisplayError: model.bindings.isCredentialsWrong)
                    .padding(.horizontal)
                
                if model.state.isCredentialsWrong {
                    HStack {
                        Spacer()
                        Text("Credenciais incorretas")
                            .foregroundColor(.red)
                            .font(.subheadline)
                    }
                    .padding(.top, 5)
                    .padding(.horizontal)
                }
                
                CustomButton(title: "ENTRAR" ,
                             action: {
                                model.userSessionService = userSessionService
                                model.submitSignIn()
                })
                .padding(.top, 25)
                .padding(.horizontal)
                
                Spacer()
            }
            .navigationBarHidden(true)
            .keyboardAdaptive()
            
            if model.state.isDataLoading {
                Rectangle()
                    .fill(Color.black.opacity(0.5))
                    .ignoresSafeArea()
                
                Image("LoadingSpinner")
            }
        }
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView(model:
                    .init(initialState:
                            .init()))
    }
}
