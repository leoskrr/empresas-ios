//
//  CustomTextField.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 26/01/21.
//

import SwiftUI

struct CustomTextField: View {
    var placeholder: String = ""
    @Binding var text: String
    var keyboardType: UIKeyboardType = .default
    var enableAutocorrection: Bool = true
    var autocapitalization: UITextAutocapitalizationType = .none
    var displaySearchIcon: Bool = false
    @Binding var shouldDisplayError: Bool

    var body: some View {
        HStack(spacing: 0) {
            if displaySearchIcon {
                Image(systemName: "magnifyingglass")
                 .padding(.leading)
                 .font(.title)
                 .foregroundColor(Color("TextFieldIcon"))
            }
            
            TextField(placeholder, text: $text)
                .padding()
                .onAppear() {
                    UITextField.appearance().tintColor = UIColor(named: "TextFieldCursor")
                }

            if shouldDisplayError {
                Image(systemName: "xmark.circle.fill")
                    .foregroundColor(.red)
                    .padding(.trailing)
            }
        }.background(
            RoundedRectangle(cornerRadius: 4)
                .fill(Color("TextFieldBackground"))
        )
        .overlay(
            RoundedRectangle(cornerRadius: 4)
                .stroke(Color.red, lineWidth: shouldDisplayError ? 1 : 0)
        )
    }
}

struct CustomTextField_Previews: PreviewProvider {
    static var previews: some View {
        CustomTextField(text: .constant(""), shouldDisplayError: .constant(false))
            .previewLayout(.sizeThatFits)
    }
}
