//
//  APIConfig.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 28/01/21.
//

import Foundation

public let apiUrl: String = "https://empresas.ioasys.com.br/api/v1"
