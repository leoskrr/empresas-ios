//
//  WebUserSessionService.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 28/01/21.
//

import Alamofire
import Combine

final class WebUserSessionService: UserSessionService, ObservableObject {
    @Published var session: UserSession?
    
    func signUpWith(email: String, password: String, completionHandler: @escaping (SignInResponse) -> ()) {
        let body: [String: String] = [
            "email": email,
            "password": password
        ]
        
        AF.request("\(apiUrl)/users/auth/sign_in", method: .post, parameters: body)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let accessToken = response.response?.allHeaderFields["access-token"] as? String,
                       let client = response.response?.allHeaderFields["client"] as? String,
                       let uid = response.response?.allHeaderFields["uid"] as? String {
                        
                        self.session = .init(accessToken: accessToken, client: client, uid: uid)
                        
                        completionHandler(.success)
                    }
                case let .failure(error):
                    if let responseCode = error.responseCode {
                        completionHandler(.error(statusCode: responseCode))
                    }
                }
            }
    }
}
