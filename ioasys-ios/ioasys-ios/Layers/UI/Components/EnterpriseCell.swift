//
//  EnterpriseCell.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 28/01/21.
//

import SwiftUI

struct EnterpriseCell: View {
    var enterpriseName: String = ""
    
    var body: some View {
        ZStack {
            VStack(spacing: 0) {
                Image("EllipseLogo")
                
                Text(enterpriseName)
                    .font(.title)
                    .bold()
                    .foregroundColor(.white)
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 150, maxHeight: 180, alignment: .center)
        .background(
            RoundedRectangle(cornerRadius: 4).fill(Color("EnterpriseBg"))
        )
    }
}

struct EnterpriseCell_Previews: PreviewProvider {
    static var previews: some View {
        EnterpriseCell().previewLayout(.sizeThatFits)
    }
}
