//
//  HomeView.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 26/01/21.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject var model: HomeViewModel
    
    init(model: HomeViewModel) {
        self.model = model
    }
    
    var body: some View {
        ZStack {
            VStack(spacing: 0) {
                ZStack(alignment: .bottom) {
                    Image("HomeBg")
                        .resizable()
                        .scaledToFill()
                        .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, minHeight: 0, idealHeight: 20, maxHeight: 200, alignment: .center)
                        .ignoresSafeArea()
                    
                    CustomTextField(placeholder: "Pesquise por empresa", text: $model.searchText, displaySearchIcon: true, shouldDisplayError: .constant(false))
                        .padding(.horizontal)
                }
                
                HStack {
                    Text(model.searchText.count > 0 && model.enterprises.count > 0
                            ? "\(model.enterprises.count) resultados encontrados"
                            : "")
                        .font(.subheadline)
                        .foregroundColor(Color("TextFieldLabel"))
                    Spacer()
                }
                .padding()
                
                Group {
                    if model.enterprises.count > 0 && model.searchText.count > 0{
                        ScrollView(.vertical, showsIndicators: false){
                            VStack {
                                ForEach(model.enterprises, id: \.id) {
                                    enterprise in
                                    
                                    NavigationLink(destination: DetailsView(enterpriseName: enterprise.enterprise_name, description: enterprise.description)) {
                                        
                                        EnterpriseCell(enterpriseName: enterprise.enterprise_name)
                                            .padding(.bottom)
                                    }
                                }
                            }
                        }
                    } else {
                        Spacer()
                        VStack {
                            Text("Nenhum resultado encontrado")
                                .foregroundColor(.gray)
                        }
                        Spacer()
                    }
                }
                Spacer()
            }
            if model.isDataLoading {
                Spacer()
                VStack {
                    Image("LoadingSpinner")
                }
                Spacer()
            }
        }
        .keyboardAdaptive()
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(model: .init())
    }
}
