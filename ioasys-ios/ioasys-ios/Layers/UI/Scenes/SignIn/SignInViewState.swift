//
//  SignInViewState.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 26/01/21.
//

import Foundation

struct SignInViewState: Equatable {
    var emailTxt: String = ""
    var passwordTxt: String = ""
    var isCredentialsWrong: Bool = false
    var isDataLoading: Bool = false
}
