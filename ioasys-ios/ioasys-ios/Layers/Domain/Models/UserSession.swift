//
//  UserSession.swift
//  ioasys-ios
//
//  Created by Leonardo Viana on 28/01/21.
//

import Foundation

struct UserSession {
    var accessToken: String
    var client: String
    var uid: String
}
